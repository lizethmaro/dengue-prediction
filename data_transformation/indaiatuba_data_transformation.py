import os
import pandas as pd

def read_data():

    list_name = [F'RESULTADO DENGUE {y}.xlsx' for y in list(range(2013, 2023))]

    df_dengue = pd.DataFrame(columns=['Bairro', 'ano', 'mes', 'dengue_diagnosis'])

    for f in list_name():
        df = pd.read_excel(os.path.join(os.getcwd(), 'data', 'indaiatuba', f), sheet_name='notificações')

        df = df[[]]
        df_dengue = pd.concat([df_dengue, df])